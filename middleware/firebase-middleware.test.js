
const firebase = require('./firebase-middleware');

let req, res, next, verifyIdToken, admin;


beforeEach(() => {

  req = {
    headers: ''
  };

  res = {
    status: jest.fn()
  };

  next = jest.fn();

  verifyIdToken = jest.fn();

  admin = {
    auth: () => ({ verifyIdToken: verifyIdToken })
  }

});

test('should return error message and error 401 if no authorization header', () => {

  firebase()(req, res, next);

  expect(next.mock.calls[0][0]).toEqual({
    code: 'auth/missing-token',
    message: 'Bearer Authorization header needed'
  });

  expect(res.status.mock.calls[0][0]).toEqual(401);

})

test('should call verifyIdToken with token from header', async () => {

  const token = 'token';

  req = {
    headers: {
      authorization: `Bearer ${token}`
    }
  };

  verifyIdToken.mockReturnValueOnce(Promise.resolve())

  await firebase(admin)(req, res, next);

  expect(verifyIdToken.mock.calls[0][0]).toEqual(token);

});

test('given token verified should call next', async () => {

  const token = 'token';

  req = {
    headers: {
      authorization: `Bearer ${token}`
    }
  };

  verifyIdToken.mockReturnValueOnce(Promise.resolve({ value: 'ok' }))

  await firebase(admin)(req, res, next);

  expect(next.mock.calls.length).toEqual(1);

});

test('given token not verified should call next with error', async () => {

  const token = 'token';

  req = {
    headers: {
      authorization: `Bearer ${token}`
    }
  };

  verifyIdToken.mockReturnValueOnce(Promise.reject({ error: 'invalid token' }))

  await firebase(admin)(req, res, next);

  expect(next.mock.calls.length).toEqual(1);
  expect(next.mock.calls[0][0]).toEqual({ error: 'invalid token' });
  expect(res.status.mock.calls[0][0]).toEqual(401);


});





