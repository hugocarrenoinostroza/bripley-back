
const firebase = admin => {

  return async function midlleware(req, res, next) {

    if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {

      res.status(401);

      next({
        code: 'auth/missing-token',
        message: 'Bearer Authorization header needed'
      });

      return;

    }

    const idToken = req.headers.authorization.split(' ')[1];

    await admin.auth().verifyIdToken(idToken)
      .then(() => {

        next();

      }).catch(error => {

        res.status(401);

        next(error);


      });

  }
}

module.exports = firebase;




