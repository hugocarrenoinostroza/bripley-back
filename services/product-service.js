const fetch = require('node-fetch');

const redis = require("redis");

const client = redis.createClient(process.env.REDIS_URL);

const getProductFromApi = async productId => {

  if (Math.random() < .15) {
    throw Error('Error from api')
  }

  const simpleRipleyUrl = `https://simple.ripley.cl/api/v2/products/${productId}`;

  const productDetail = await fetch(simpleRipleyUrl);

  const response = await productDetail.text();

  //Logging response text for heroku
  //console.log(response);

  return JSON.parse(response);

}

const getProductDetail = (productId, res, retries) => {

  client.get(productId, async function (err, reply) {

    if (!reply) {

      try {

        const json = await getProductFromApi(productId);

        console.log('Getting product info from api: ', json);

        client.set(productId, JSON.stringify(json), 'EX', 120);

        res.json(json);

      } catch (error) {

        if (!retries) {
          retries = 0;
        }

        if (retries < 5) {
          console.log(`Error while getting product info from api, trying again in 5 seg, retry ${retries}`, error);

          setTimeout(() => getProductDetail(productId, res, ++retries), 5000);
        }



      }

    } else {

      const json = JSON.parse(reply);

      console.log('Getting product info from cache: ', json);

      res.json(json);

    }

  });

};

module.exports = getProductDetail;
