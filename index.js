const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000

const cors = require('cors')

const auth = require('./middleware/firebase-middleware');

const admin = require("firebase-admin");


admin.initializeApp({
  credential: admin.credential.cert(
    JSON.parse(process.env.FIREBASE_CONFIG)
  )
});

const products = require('./routers/products');

const errorHandler = (err, req, res, next) => {

  if (err) {
    res.json({ error: err });
  }

  if (!err.status) {
    res.status(500);
  }

}

app.use(cors())

app.use(auth(admin));

app.use(products);

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}`);
})
