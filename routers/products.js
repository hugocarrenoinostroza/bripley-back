
const express = require('express');
const router = express.Router();

const routerFunctions = require('../routers-functions/router-functions');

router.get('/products', routerFunctions.getProducts);

router.get('/products/:id', routerFunctions.getProductsById);

module.exports = router;
