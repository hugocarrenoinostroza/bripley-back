
const productService = require('../services/product-service');

const getProducts = (req, res) => {

  res.status(200).json(
    [
      {
        id: '2000377739068P',
        name: 'HP',
        description: 'HP 14-DQ1004LA / INTEL® CORE™ I5 / 24GB MEMORIA (16GB INTEL® OPTANE™ + 8GB RAM) / 256GB SSD / GRAFICAS INTEL® UHD / 14"',
        price: 519990,
        img: 'https://home.ripley.cl/store/Attachment/WOP/D113/2000377739068/2000377739068-1.jpg'
      },
      {
        id: '2000379805204',
        name: 'ASUS',
        description: 'ASUS LAPTOP M409D / AMD RYZEN™ 3 / 4GB RAM / 1TB / AMD RADEON VEGA 3 / 14""',
        price: 449990,
        img: 'https://home.ripley.cl/store/Attachment/WOP/D113/2000379805204/2000379805204-1.jpg'
      },
      {
        id: '2000375197631P',
        name: 'LENOVO',
        description: 'LENOVO IDEAPAD L340 GAMING / INTEL® CORE™ I5 / 8GB RAM / 1TB + 128GB SSD / NVIDIA GEFORCE GTX1650 4GB / 15.6" FHD""',
        price: 749990,
        img: 'https://home.ripley.cl/store/Attachment/WOP/D113/2000375197631/2000375197631-1.jpg'
      },
      {
        id: '2000379236237P',
        name: 'MACBOOK',
        description: 'MACBOOK PRO RETINA TBAR / INTEL® CORE™ I5 / 16GB RAM / 512SSD / INTEL® IRIS® PLUS 645 / 13.3""',
        price: 2299990,
        img: 'https://home.ripley.cl/store/Attachment/WOP/D113/2000379236237/2000379236237_2.jpg'
      }
    ]
  )

}

const getProductsById = (req, res) => {

  const id = req.params.id;

  productService(id, res);

}

exports.getProducts = getProducts;
exports.getProductsById = getProductsById;


